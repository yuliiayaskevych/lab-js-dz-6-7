import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Family } from '../../models';
import { FamilyService } from '../../services/family.service';

@Component({
  selector: 'lab-js-add-family',
  templateUrl: './add-family.component.html',
  styleUrls: ['./add-family.component.scss']
})
export class AddFamilyComponent implements OnInit {
  public familyForm: FormGroup;
  public get children() {
    return this.familyForm.get('children') as FormArray;
  }
  public constructor(
    private readonly familyService: FamilyService
  ) {

  }
  public ngOnInit(): void {
    this.initForm();
  }

  private initForm() {
    this.familyForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      father: this.generateFormGroup(),
      mother: this.generateFormGroup(),
      children: new FormArray([
        this.generateFormGroup()
      ])
    })
  }
  public addChild() {
    const child = this.generateFormGroup();
    (this.familyForm.get('children') as FormArray).push(child);
  }
  public removeChild(index: number) {
    (this.familyForm.get('children') as FormArray).removeAt(index)
  }
  public submit() {
    const family: Family = { ...this.familyForm.value };
    this.familyService.addFamily$(family).subscribe();
  }

  private generateFormGroup() {
    return new FormGroup({
      name: new FormControl(null, [Validators.required]),
      age: new FormControl(null, [Validators.required]),
    });
  }
}
